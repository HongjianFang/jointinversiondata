# README #
Joint inversion code and an example of SJFZ.

### What is this repository for? ###

* bake up
* keep maintaining
* collaborate with others

### How do I get set up? ###

* compile the code in the similar way as tomoDD
* remember to change some parameters in include/*inc file according to model dimensions
* also need to note the parameters for surface wave data the surf_tomo.in

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* contact hjfang.geo@gmail.com if you find any bug
* or zhang11@ustc.edu.cn